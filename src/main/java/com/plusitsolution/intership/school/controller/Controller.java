package com.plusitsolution.intership.school.controller;

import com.plusitsolution.intership.school.domain.Student;
import com.plusitsolution.intership.school.es.StudentEntity;
import com.plusitsolution.intership.school.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
public class Controller {

    @Autowired
    private StudentService service;

    @GetMapping("/ping")
    public String ping() {
        return "OK";
    }

    @GetMapping("/getAllStudent")
    public List<StudentEntity> getAllStudent() {
        return service.getAllStudent();
    }

    @PostMapping("/getStudentByname")
    public StudentEntity getStudentByname(@RequestParam("name") String name) {
        return service.getStudentByname(name);
    }

    @PostMapping("/getStudentByAgeGreaterThanEqual")
    public List<StudentEntity> getStudentByAgeGreaterThanEqual(@RequestHeader("age") int age) {
        return service.getStudentByAgeGreaterThanEqual(age);
    }

    @PutMapping("/addStudent")
    public StudentEntity addStudent(@RequestBody Student student) {
        return service.addStudent(student);
    }
}
