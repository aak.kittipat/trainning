package com.plusitsolution.intership.school.domain;

import com.plusitsolution.intership.school.es.StudentEntity;

public class Student {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public StudentEntity toEntity() {
        StudentEntity entity = new StudentEntity();
        entity.setName(name);
        entity.setAge(age);
        return entity;
    }
}
