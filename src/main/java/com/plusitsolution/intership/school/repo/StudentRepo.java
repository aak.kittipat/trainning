package com.plusitsolution.intership.school.repo;

import com.plusitsolution.intership.school.domain.Student;
import com.plusitsolution.intership.school.es.StudentEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.time.LocalDate;
import java.util.List;

public interface StudentRepo extends ElasticsearchRepository<StudentEntity, String> {
    List<StudentEntity> findAll();
    StudentEntity findByName(String name);
    List<StudentEntity> findByAgeGreaterThanEqual(int age);
}
