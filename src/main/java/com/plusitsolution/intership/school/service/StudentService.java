package com.plusitsolution.intership.school.service;

import com.plusitsolution.intership.school.domain.Student;
import com.plusitsolution.intership.school.es.StudentEntity;
import com.plusitsolution.intership.school.repo.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepo repo;

    @Autowired
    private void init() {
        repo.deleteAll();
        List<StudentEntity> entities = new ArrayList<>();

        StudentEntity entity = new StudentEntity();
        entity.setName("Momo");
        entity.setAge(13);
        entities.add(entity);

        entity = new StudentEntity();
        entity.setName("Mimi");
        entity.setAge(14);
        entities.add(entity);

        entity = new StudentEntity();
        entity.setName("Meme");
        entity.setAge(15);
        entities.add(entity);

        entity = new StudentEntity();
        entity.setName("Mama");
        entity.setAge(16);
        entities.add(entity);
        repo.saveAll(entities);
    }

    public List<StudentEntity> getAllStudent() {
        return repo.findAll();
    }

    public StudentEntity getStudentByname(String name) {
        return repo.findByName(name);
    }

    public List<StudentEntity> getStudentByAgeGreaterThanEqual(int age) {
        return repo.findByAgeGreaterThanEqual(age);
    }

    public StudentEntity addStudent(Student student) {
        return repo.save(student.toEntity());
    }
}
